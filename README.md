### Louen PESCHARD
### Théo LOURY
### Nicolas MARETTE
### Théo JOUAN


## Explications

- Il faut lancer le fichier game.py
- Nous avons fait un tour rouge et un tour bleu (il aurait fallut réitérer jusqu'à un vainqueur / perdant)
- L'IA fait deviner forcément 2 mots pour l'instant
- Nous avons essayer de tester d'autres modèles (voir le fichier tests)
- Afin d'améliorer nos résultats, nous avons retiré les mots blanc des mots négatifs pour le modèle, c'est ce qui a rendu les résulats nettment meilleurs
- Nous avons également rajouté un tri supplémentaire des réultats, en comparant les comparant avec les 2 mots à identifier. Ainsi il ne propose pas forcément le 1e mot de la liste des résultats.
