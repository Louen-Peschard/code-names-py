import random
import numpy as np
import matplotlib.pyplot as plt
from gensim.models.fasttext import load_facebook_vectors

file = open("datas/codenames.txt").readlines()
words = ([s.replace('\n', '') for s in file])
# Afficher tous les mots du fichier codenames.txt
# print(words)
print("nombre de mots au total : ", len(words))

random.shuffle(words)  # Mélanger la liste de manière aléatoire
words = words[:25]
words = list(map(str.lower, words))

red_words = words[:9]
blue_words = words[9:17]
white_words = words[17:24]
black_words = words[24:25]

print("les mots rouges sont : ", red_words)
print("les mots bleus sont : ", blue_words)
print("les mots blancs sont : ", white_words)
print("le mot noir est : ", black_words)

# Création de la grille
grid = np.random.randint(4, size=(5, 5))

# Création du graphique
fig, ax = plt.subplots()

random.shuffle(words)

n = 0
# Dessin des rectangles et ajout des labels
for i in range(len(grid)):
    for j in range(len(grid[i])):
        if words[n] in red_words:
            color = "#f36722"
        elif words[n] in blue_words:
            color = "#01abc4"
        elif words[n] in white_words:
            color = "#f5d9b6"
        else:
            color = "#949494"
        rect = plt.Rectangle((j, i), 1, 1, facecolor=color)
        ax.add_patch(rect)
        ax.text(j + 0.5, i + 0.5, words[n], ha='center', va='center')
        n = n + 1

# Configuration du graphique
ax.set_xlim([0, 5])
ax.set_ylim([0, 5])
ax.set_aspect('equal')
ax.axis('off')

# Affichage de la grille
plt.show()

### IA
# Chemin vers le modèle Fasttext pré-entraîné
path = "datas/fasttext/cc.fr.300.bin"

# Charger le modèle
model = load_facebook_vectors(path)

# Trouver les 10 mots les plus similaires pour la liste de mots donnée
similar_words = model.most_similar(positive=red_words,
                                   negative=blue_words + white_words +
                                   black_words,
                                   topn=10)

# Afficher les résultats
for word, similarity in similar_words:
    print(word, similarity)

score = []
for i in range(len(red_words)):
    for j in range(i, len(red_words)):
        if i != j:
            similarite = model.similarity(red_words[i], red_words[j])
            print(red_words[i], "+", red_words[j], "=", similarite)
            score.append([similarite, red_words[i], red_words[j]])

sorted_scores = sorted(score, key=lambda x: x[0], reverse=True)
sorted_scores = [element for element in sorted_scores if element[0] >= 0.12]

score = []
for i in range(len(sorted_scores)):
    for j in range(len(red_words)):
        if red_words[j] not in sorted_scores[i][1:]:
            similarite = model.n_similarity(sorted_scores[i][1:], red_words[j])
            words = []
            for word in sorted_scores[i][1:]:
                words.append(word)
            words.append(red_words[j])
            score.append([similarite, words])

sorted_scores.append(sorted(score, key=lambda x: x[0], reverse=True))

model.most_similar(positive=red_words,
                   negative=blue_words + white_words + black_words,
                   topn=10)

resultats = model.most_similar(positive=sorted_scores[0][1:],
                               negative=blue_words + white_words + black_words,
                               topn=10)
resultats = [mot[0] for mot in resultats]

print("L'IA propose ", resultats[0], len(sorted_scores[0][1:]),
      " pour trouver les mots :", sorted_scores[0][1:])