import random
import numpy as np
import matplotlib.pyplot as plt
from gensim.models import KeyedVectors
# import spacy
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

file = open("inputs/mots-codenames/codenames.txt").readlines()
words = ([s.replace('\n', '') for s in file])
# print(words)

random.shuffle(words)  # Mélanger la liste de manière aléatoire
words = words[:25]
words = list(map(str.lower,words))
words_remaining = words

red_words = words[:9]
blue_words = words[9:17]
white_words = words[17:24]
black_words = words[24:25]

print("les mots rouges sont : ",red_words)
print("les mots bleus sont : ",blue_words)
print("les mots blancs sont : ",white_words)
print("le mot noir est : ",black_words)

# Création de la grille
grid = np.random.randint(4, size=(5, 5))

# Création du graphique
fig, ax = plt.subplots()

random.shuffle(words)

n=0
# Dessin des rectangles et ajout des labels
for i in range(len(grid)):
    for j in range(len(grid[i])):
        if words[n] in red_words:
            color = "#f36722"
        elif words[n] in blue_words:
            color = "#01abc4"
        elif words[n] in white_words:
            color = "#f5d9b6"
        else :
            color = "#949494"
        rect = plt.Rectangle((j, i), 1, 1, facecolor=color)
        ax.add_patch(rect)
        ax.text(j + 0.5, i + 0.5, words[n], ha='center', va='center')
        n=n+1

# Configuration du graphique
ax.set_xlim([0, 5])
ax.set_ylim([0, 5])
ax.set_aspect('equal')
ax.axis('off')

# Affichage de la grille
plt.show()


# Chemin vers le modèle Word2Vec pré-entraîné
path = "inputs/word2vecfr/frWac_no_postag_no_phrase_500_cbow_cut100.bin"

# Charger le modèle
model = KeyedVectors.load_word2vec_format(path, binary=True)
hint = ""
def give_hint(color):
    score =[]
    
    if color == "red":
        words_to_find = red_words
        words_to_avoid = blue_words
    else:
        words_to_find = blue_words
        words_to_avoid = red_words
        
    for i in range(len(words_to_find)):
        for j in range(i,len(words_to_find)):
            if i!=j:
                similarite = model.similarity(words_to_find[i], words_to_find[j])
                score.append([similarite,words_to_find[i],words_to_find[j]])

    sorted_scores = sorted(score, key=lambda x: x[0],reverse = True)
    sorted_scores = [element for element in sorted_scores if element[0] >= 0.12]

    score =[]
    for i in range(len(sorted_scores)):
        for j in range(len(words_to_find)):
            if words_to_find[j] not in sorted_scores[i][1:]:
                similarite = model.n_similarity(sorted_scores[i][1:], words_to_find[j])
                words=[]
                for word in sorted_scores[i][1:]:
                    words.append(word)
                words.append(words_to_find[j])
                score.append([similarite,words])
                            
    sorted_scores.append(sorted(score, key=lambda x: x[0],reverse = True))

    model.most_similar(positive=words_to_find,negative=words_to_avoid+black_words, topn=10)

    resultats = model.most_similar(positive=sorted_scores[0][1:],negative=words_to_avoid+black_words, topn=10)

    resultats = [mot[0] for mot in resultats]

    # Tri supplémentaire
    print("---------------------")
    max = -20
    final_word = ""
    for resultat in resultats:
        value = 0
        for i in range(len(sorted_scores[0][1:])):
            similarite = model.similarity(sorted_scores[0][1:][i], resultat)
            value += similarite
        if max < value:
            max = value
            final_word = resultat

    hint = final_word

    print("L'indice est : ", hint, "pour trouver : ", sorted_scores[0][1:])
    return hint

def choice(choice = ""):
    words_remaining.remove(choice)
    if choice in red_words:
        red_words.remove(choice)
        return "red"
    elif choice in blue_words:
        blue_words.remove(choice)
        return "blue"
    elif choice in white_words:
        white_words.remove(choice)
        return "white"
    else:
        black_words.remove(choice)
        return "black"

def guess(indice):
    wordfind = []
    bestresult = ''
    j = 0

    score = []
    for i in range(len(words_remaining)) :
        wordfind = model.similarity(words_remaining[i], indice)
        if(wordfind >= j):
            j = wordfind
            bestresult = words_remaining[i]
        score.append([wordfind,words_remaining[i],indice])

    if bestresult == "":
        return "vide"
    print("L'IA devine le mot : " + bestresult)
    return choice(bestresult)

def tour(color):
    # Note à soi-même : Oui c'est moche, mais fallait pas faire du franglais aussi
    if color == "red":
        french_color_for = "rouge"
        french_color_against = "bleu"
    else:
        french_color_for = "bleu"
        french_color_against = "rouge"
    hint = give_hint(color)
    for i in range(2):
        response_color = guess(hint)
        if response_color == "vide":
            print("L'IA n'a pas trouvé de mots proches, on passe le tour")
            break
        elif response_color == "black":
            print("Le mot est noir ! Perdu !")
            break
        elif response_color == "white":
            print("Le mot est blanc ! On passe le tour")
            break
        elif response_color != color:
            print("Le mot est "+french_color_against+" ! On passe le tour")
            break
        elif i != 1:
            print("Le mot est "+french_color_for+" ! On continue !")
        else:
            print("Le mot est "+french_color_for+" ! Plus de tentative restante")

print("---Tour rouge---")
tour("red")
print("---Tour bleu---")
tour("blue")